﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2Parcial2
{
    class Myexception:System.Exception
    {
       
            public Myexception()
            {

            }

            public Myexception(string mensaje) : base(mensaje)
            {

            }

            public Myexception(string mensaje, Exception InnerE) : base(mensaje, InnerE)
            {

            }

        
    }
}
